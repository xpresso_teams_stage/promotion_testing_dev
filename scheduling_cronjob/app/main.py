""" Marketplace component for executing a scheduled task in xpresso """

__author__ = "Sahil Malav"

import datetime
import sys
import json
import os
from kubernetes import client
from kubernetes.client.rest import ApiException
from xpresso.ai.core.commons.utils.generic_utils import str2bool, \
    modify_string_for_deployment
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidValueException, ClusterNotFoundException
from xpresso.ai.client.controller_client import ControllerClient
from xpresso.ai.server.controller.persistence.persistentce_connection import \
    create_persistence_object


class TaskManager:

    def __init__(self):
        self.logger = XprLogger("execute_task")
        action = json.loads(str(sys.argv[1]))
        self.action_type = action['type']
        self.action_parameters = action['parameters']
        self.is_recurring = str2bool(str(sys.argv[2]))
        self.client = ControllerClient()
        self.schedule_name = os.environ['SCHEDULE_NAME']
        self.cronjob_namespace = 'xpresso-schedules'
        self.project_name = os.environ['PROJECT_NAME']
        self.cronjob_name = modify_string_for_deployment(
            f'xs--{self.schedule_name}--{self.project_name}')

    def execute(self):
        """ Executes the scheduled task """
        self.client.login('xprsu', 'Xpresso@Abz00ba2019#@!')
        if self.action_type == 'run_pipeline' or \
                self.action_type == 'start_experiment':
            new_run_name = self.generate_run_name(self.schedule_name)
            self.action_parameters['run_name'] = new_run_name
            response = self.client.start_experiment(self.action_parameters)
        elif self.action_type == 'deploy_project':
            response = self.client.deploy_project(self.action_parameters)
        elif self.action_type == 'build_project':
            response = self.client.build_project(self.action_parameters)
        else:
            self.logger.error(
                f'Invalid action type "{self.action_type}" provided.')
            raise InvalidValueException(
                f'Invalid action type "{self.action_type}" provided.')
        print(f'\n\n{response}\n')
        if not self.is_recurring:
            print('\nNon-recurring task completed. '
                  'Proceeding to delete the cronjob...\n')
            self.delete_cronjob()
        else:
            print(f'\nTask completed.\nThis is a recurring task and will be '
                  f'triggered again at the next scheduled time.\n\n')

    def delete_cronjob(self):
        """ deletes the cronjob for one-time schedules """
        try:
            master_node = os.environ['MASTER_NODE']
            self.setup_api_config(master_node)
            k8s_beta = client.BatchV1beta1Api()
            api_response = k8s_beta.delete_namespaced_cron_job(
                name=self.cronjob_name,
                namespace=modify_string_for_deployment(self.cronjob_namespace)
            )
            self.logger.debug(f"Cronjob deleted. Details: {str(api_response)}")
            print('\nCronjob deleted successfully.\n')
        except (ApiException, KeyError) as e:
            self.logger.error(f'Deletion of cronjob failed. Reason: {str(e)}')

    def setup_api_config(self, master_node):
        """
        sets api config for kubernetes
        Args:
            master_node (str): IP of master node
        """
        config = XprConfigParser()
        persistence_manager = create_persistence_object(config)
        # make sure mongo pwd is hardcoded in common.json and common_stage.json
        master_info = persistence_manager.find('nodes',
                                               {"address": master_node})
        # get the kubernetes bearer token from master
        try:
            token = master_info[0]['token']
        except (IndexError, KeyError):
            self.logger.error("Failed to fetch token for setting up "
                              "API configurations.")
            raise ClusterNotFoundException(
                "Failed to fetch token for setting up API configurations. "
                "Please contact an Xpresso developer.")
        self.logger.debug('Bearer token retrieved from master node')
        configuration = client.Configuration()
        configuration.host = f'https://{master_node}:6443'
        configuration.verify_ssl = False
        configuration.debug = True
        configuration.api_key = {"authorization": "Bearer " + token}
        client.Configuration.set_default(configuration)
        self.logger.debug('API configurations set.')

    def generate_run_name(self, schedule_name):
        """
        Generates run name for a scheduled pipeline run
        Args:
            schedule_name(str): name of the schedule

        Returns(str): generated name

        """
        current_datetime_stamp = \
            datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        generated_run_name = \
            f'scheduled_run_{schedule_name}_{current_datetime_stamp}'
        self.logger.debug(f'Run name generated : {generated_run_name}')
        return generated_run_name


if __name__ == '__main__':
    TaskManager().execute()

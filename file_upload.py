import requests
import os

# xpresso credentials
uid = "ralph.maison"
pwd = ""
project = "portfolio_monitor"
pipeline = "portf_monitor"
rds_folder  = 'C:/Users/sahil.malav/Documents/rds'



####### DO NOT CHANGE THE CODE BELOW ##########
auth_url = 'https://aviva-cc-dev.xpresso.ai:8001/api/auth'
data = {"uid": uid, "pwd": pwd}
r = requests.post(url=auth_url, json=data)
token = r.json()['results']['access_token']

url = f"https://aviva-cc-dev.xpresso.ai:8001/nfs/upload_files/data1/exports/vmdata1/xpresso_platform_sb/projects/{project.replace('_', '-')}/pipelines/{pipeline.replace('_', '-')}"


files = os.listdir(rds_folder)
for file in files:
    file_path = os.path.join(rds_folder, file)
    payload={'base_path': '',
    'api_flag': 'nfs_explorer',
    'name': project}
    files=[
      ('file',open(file_path,'rb'))
    ]
    headers = {
      'token': token
    }
    
    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    
    print(response.text)
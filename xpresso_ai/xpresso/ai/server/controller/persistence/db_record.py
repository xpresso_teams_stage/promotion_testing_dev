class DBRecordObject(object):
    """
    Base class for an object on xpresso platform that describes a record in db
    """
    def __init__(self, collection: str, record_info: dict):
        """
        Constructor method for db
        Args:
            collection: name of the collection
            record_info: a dictionary with info on the record
        """
        self.collection = collection
        self.data = record_info

    def set(self, key, value):
        """
        setter method
        Args:
            key: key to set the value for
            value: value of the record
        """
        self.data[key] = value

    def get(self, key):
        """
        get the value from db record
        Args:
            key: key of the item to return
        Returns:
             returns the value of a key in db
        """
        return self.data[key]

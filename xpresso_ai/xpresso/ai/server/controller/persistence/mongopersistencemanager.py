import pymongo
from pymongo import MongoClient
from pymongo.database import Database
from pymongo.errors import PyMongoError, OperationFailure
from pymongo.errors import DuplicateKeyError
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    UnsuccessfulConnectionException, PrimaryKeyException, UnsuccessfulOperationException
from xpresso.ai.core.commons.utils.singleton import Singleton

import time
from xpresso.ai.core.logging.xpr_log import XprLogger
from enum import Enum, unique

MONGO_RECORD_ID = "_id"

# enumeration class for database operations
@unique
class DBOperation(Enum):
    INSERT = 1
    UPDATE = 2
    REPLACE = 3
    FIND = 4
    DELETE = 5


# design issues: repeated connects are expensive (approx 3 seconds each)
# consequently, once a database connection is established, it should be re-used
# there are 2 ways to achieve this:
# (a) make this class a Singletone
# (b) make the database connection static
# both have repercussions in case of multiple threads
# in general, (b) has been chosen here because only usage of the database connection object needs to be made
# thread-safe,
# as opposed to (a) where each and every method of this class needs to be made thread-safe
# pymongo claims to be thread-safe, hence keeping the connection object static should be OK

class MongoPersistenceManager(metaclass=Singleton):
    # code for singleton class
    '''
    class __Singleton:
        def __init__(self):
            pass

        def __str__(self):
            return repr(self) + self.val

    '''
    """
    This class performs various database (CRUD) operations on a Mongo DB database.
    Attributes:
        url (str) - URL of the api_server
        persistence (str) - name of the database
        uid (str) - user id
        pwd (str) - password
        w (int) - write concern (default = 0)
    """
    config_path = XprConfigParser.DEFAULT_CONFIG_PATH
    config = XprConfigParser(config_path)
    INTERVAL_BETWEEN_RETRIES = int(
        config["mongodb"]["interval_between_retries"])
    MAX_RETRIES = int(config["mongodb"]["max_retries"])
    logger = XprLogger()

    # singleton instance
    # instance = None

    # (static) database connection
    mongo_db = None

    # def __getattr__(self, name):
    #    return getattr(self.instance, name)

    def __init__(self, url: str, db: str, uid: str, pwd: str, w: int = 1):
        """
        Constructor.
        :param url (str): Mongo DB api_server URL
        :param persistence (str): name of database to perform operations on
        """
        self.logger.debug(
            "Entering MongoPersistenceManager constructor with parameters url %s, persistence %s, uid %s, pwd %s, w %s" % (
                url, db, uid, pwd, w))
        self.url = url
        self.db = db
        self.uid = uid
        if len(self.uid) == 0:
            self.uid = None
        self.pwd = pwd
        self.w = w
        self.logger.debug("Created MongoPersistenceManager object successfully")
        self.logger.debug("Exiting constructor")

    def connect(self) -> Database:
        """
        Connects to a specific database of a api_server.
        :return: Mongo Client object and database connection

        """
        self.logger.debug("Entering connect method")
        self.logger.debug("Checking if connection already active")
        if self.mongo_db is None:
            self.logger.debug(
                "Attempting connection to database %s on api_server %s" % (
                    self.db, self.url))
            # connect to api_server
            mongo_client = MongoClient(host=self.url, w=self.w, maxPoolSize=4, waitQueueTimeoutMS=10)
            self.logger.debug("Created Mongo client object")

            # Note Starting with version 3.0 the MongoClient constructor no longer blocks while connecting to the
            # api_server or servers, and it no longer raises ConnectionFailure if they are unavailable,
            # nor ConfigurationError if the userâ€™s credentials are wrong. Instead, the constructor returns
            # immediately and launches the connection process on background threads. make sure connection has been
            # established
            connected = False
            attempt = 1
            while not connected and attempt <= self.MAX_RETRIES:
                try:
                    # The ismaster command is cheap and does not require auth.
                    self.logger.debug("Checking connection to api_server")
                    mongo_client.admin.command('ismaster')
                    connected = True
                    self.logger.debug("Connected to api_server successfully")
                except UnsuccessfulConnectionException:
                    # TBD: LOG
                    self.logger.debug(
                        "Server not available: waiting for connection. Attempt %s of %s"
                        % (attempt, self.MAX_RETRIES))
                    # wait INTERVAL_BETWEEN_RETRIES seconds and retry MAX_RETRIES times
                    time.sleep(self.INTERVAL_BETWEEN_RETRIES)
                    attempt += 1
            if not connected:
                self.logger.error(
                    "Unable to connect to database after %s attempts" % self.MAX_RETRIES)
                raise UnsuccessfulConnectionException(
                    "Unable to connect to database")

            self.logger.debug("Connected to api_server successfully")
            # get database pointer
            self.logger.debug("Connecting to database %s" % self.db)
            MongoPersistenceManager.mongo_db = mongo_client[self.db]
            if MongoPersistenceManager.mongo_db is None:
                # TBD: LOG
                raise UnsuccessfulConnectionException(
                    "Unknown database %s" % self.db)

            self.logger.debug(
                "Connected to database successfully. Authenticating user")
            # authenticate user
            try:
                if (self.uid is not None) and (self.pwd is not None):
                    MongoPersistenceManager.mongo_db.authenticate(self.uid,
                                                                  self.pwd)
            except PyMongoError:
                self.logger.debug(("Invalid user ID %s or password" % self.uid))
                raise UnsuccessfulConnectionException(
                    "Invalid user ID %s or password" % self.uid)

            # return database pointer
            self.logger.debug("Authentication successful")
        else:
            self.logger.debug("Connection already active")
        self.logger.debug(
            "Exiting connect method with return value %s" % MongoPersistenceManager.mongo_db)
        return MongoPersistenceManager.mongo_db

    def disconnect(self, mongo_client: MongoClient):
        """
        disconnects from the database
        """
        self.logger.debug(
            "Entering disconnect method with parameters %s" % mongo_client)
        try:
            # close connection
            # TBD: LOG
            mongo_client.close()
        except UnsuccessfulConnectionException:
            # do nothing - no point throwing exception if problem closing connection
            self.logger.error(
                "Connection failure while trying to disconnect from %s, %s" % (
                    self.url, self.db))
            pass
        self.logger.debug("Disconnected sucessfully")
        self.logger.debug("Exiting disconnect method")

    def create_index(self):
        """
        Creates indexes for all the collections present in db
        """
        try:
            mongo_db = self.connect()
            index_details = self.config["mongodb"]["index"]
            config_collections = index_details.keys()
            for collection_name in config_collections:
                if "key_name" in index_details[collection_name].keys() and \
                        "index_name" in index_details[collection_name].keys():
                    keys = index_details[collection_name]['key_name']
                    key_list = list()
                    for key in keys:
                        if key[-1] == 1:
                            key_list.append((key[0], pymongo.ASCENDING))
                        elif key[-1] == -1:
                            key_list.append((key[0], pymongo.DESCENDING))
                    index_name = index_details[collection_name]['index_name']
                    db_collection = mongo_db[collection_name]
                    db_collection_index = db_collection.index_information()
                    if index_name not in db_collection_index.keys():
                        db_collection.create_index(keys=key_list, name=index_name)
                    else:
                        self.logger.debug(f"Index already created for {collection_name} with index name {index_name}")
                else:
                    raise KeyError
        except OperationFailure as err:
            self.logger.debug(err)


    def insert(self, collection: str, obj, duplicate_ok: bool) -> str:
        """

        :param collection: (string) collection to insert into
        :param obj: (dict) object to be inserted
        :param duplicate_ok: (bool) true if object can be inserted even if it already exists

        :return: ID of the inserted / updated object
        """
        self.logger.debug(
            "Entering insert method with parameters collection: %s, obj: %s, duplicate_ok: %s"
            % (collection, obj, duplicate_ok))
        doc_id = None
        # if duplicate_ok == false, check if the object exists
        # assumption: unique index exists in collection, hence no need to check for duplicates
        # duplicate_ok is ignored
        # add_object = duplicate_ok
        ''' if not duplicate_ok:
            self.logger.debug(
                "Duplicates not allowed. Checking if object exists already")
            kwargs = {}
            kwargs["filter"] = obj
            self.logger.debug("Calling perform_db_operation for find operation")
            obj_found = self.perform_db_operation(collection, DBOperation.FIND,
                                                  **kwargs)
            doc_id = -1
            if obj_found is not None:

                try:
                    doc_id = obj_found[0]["_id"]
                    doc_id = -1
                    self.logger.debug("Object exists already. Not inserting")
                except IndexError:
                    add_object = True
                    self.logger.debug("Object does not exist. Inserting")
            else:
                self.logger.debug("Duplicates allowed. Inserting object")
                add_object = True
            obj_found.close()'''
        # if add_object:
        kwargs = obj
        self.logger.debug(
            "Calling perform_db_operation for insert operation")
        result = self.perform_db_operation(collection, DBOperation.INSERT,
                                           **kwargs)
        doc_id = result.inserted_id
        self.logger.debug(
            "Insert successful. New document ID is %s" % doc_id)

        self.logger.debug("Exiting insert method with return value %s" % doc_id)
        return doc_id

    def update(self, collection: str, doc_filter, obj, flag: str = None, upsert: bool = False):
        """

        :param collection: (string) name of collection to update
        :param doc_filter: filter for the object to be updated
        :param obj: new attributes of the object to be updated
        :param upsert: (bool) true if object to be inserted if not found
        :param flag: type of operator for update
        :return: ID of the object updated
        """
        self.logger.debug(
            "Entering update method with parameters collection: %s, doc_filter: %s, obj: %s, upsert: %s"
            % (collection, doc_filter, obj, upsert))
        kwargs = {}
        kwargs["filter"] = doc_filter
        update = {}
        if flag == "pull":
            update["$pull"] = obj
        else:
            update["$set"] = obj
        kwargs["update"] = update
        kwargs["upsert"] = upsert
        self.logger.debug("Calling perform_db_operation for update operation")
        result = self.perform_db_operation(collection, DBOperation.UPDATE,
                                           **kwargs)
        doc_id = result.upserted_id
        self.logger.debug("Update successful. Document ID: %s" % doc_id)

        self.logger.debug("Exiting update method with return value %s" % doc_id)
        return doc_id

    def pull(self, collection: str, doc_filter):
        self.logger.debug(
            "Pulling document with parameters collection: %s, doc_filter: %s"
            % (collection, doc_filter))
        kwargs = {"filter": doc_filter}
        result = self.perform_db_operation(collection, DBOperation.UPDATE,
                                           **kwargs)
        doc_id = result.upserted_id
        return doc_id

    def replace(self, collection: str, doc_filter, obj, upsert: bool = False):
        """

        :param collection: (str) name of collection in which to replace document
        :param doc_filter: filter for the object to be replaced
        :param obj: new attributes of the object to be replaced
        :param upsert: (bool) true if object to be inserted if not found
        :return: ID of the object replaced
        """
        self.logger.debug(
            "Entering replace method with parameters collection: %s, doc_filter: %s, obj: %s, upsert: %s"
            % (collection, doc_filter, obj, upsert))
        kwargs = {}
        kwargs["filter"] = doc_filter
        kwargs["replacement"] = obj
        kwargs["upsert"] = upsert
        self.logger.debug("Calling perform_db_operation for replace operation")
        result = self.perform_db_operation(collection, DBOperation.REPLACE,
                                           **kwargs)
        doc_id = result.upserted_id
        self.logger.debug("Replace successful. Document ID: %s" % doc_id)

        self.logger.debug("Exiting replace method with return value %s" % doc_id)
        return doc_id

    def find(self, collection: str, doc_filter, proj_filter=None, limit=1000):
        """
        finds one or more documents in the collection matching the specified filter
        :param collection: (str)  to be searched
        :param doc_filter: (dict) query to be applied
        :param proj_filter: (dict) projection to get as per user requirement
        :param limit: (int) number of entries that needs to returned from db
        :return: (array of dict) document(s) found, or None
        """
        self.logger.debug(
            "Entering fnd method with parameters collection: %s, doc_filter: %s"
            % (collection, doc_filter))

        kwargs = {"filter": doc_filter, "projection": proj_filter}
        if limit is not None:
            kwargs["limit"] = limit
        self.logger.debug("Calling perform_db_operation for find operation")
        result = self.perform_db_operation(collection, DBOperation.FIND,
                                           **kwargs)
        # self.logger.debug(
        #    "Operation completed. Results: %s. Converting to object array" % result)

        # convert result from cursor to array of dict
        final_res = []

        for record in result:
            if MONGO_RECORD_ID in record:
                del record[MONGO_RECORD_ID]
            final_res.append(record)

        result.close()
        # self.logger.debug("Conversion complete. Results: %s" % final_res)
        # self.logger.debug("Exiting find method with return value %s" % final_res)
        return final_res

    def delete(self, collection: str, doc_filter):
        """
        deletes documents from a collection that match the specified filter
        :param collection: (str) collection from which document(s) is/are to be deleted
        :param doc_filter: query to be applied to find documents
        :return: number of documents deleted
        """
        self.logger.debug(
            "Entering delete method with parameters collection: %s, doc_filter: %s"
            % (collection, doc_filter))
        kwargs = {}
        kwargs["filter"] = doc_filter
        self.logger.debug("Calling perform_db_operation for delete operation")
        result = self.perform_db_operation(collection, DBOperation.DELETE,
                                           **kwargs)

        self.logger.debug(
            "Operation successful. %s records deleted" % result.deleted_count)
        self.logger.debug(
            "Exiting delete method with return value %s" % result.deleted_count)
        return result.deleted_count

    def perform_db_operation(self, collection: str, operation: str, **kwargs):
        """
        performs a database operation - guarantees success within N retries (throws UnsuccessfulOperationException
        if unsuccessful after N retries)
        :param collection: (str) name of collection to be operated on
        :param operation: (str) name of operation to be performed
        :param kwargs: arguments for operation
        :return:
        """
        self.logger.debug(
            "Entering perform_db_operation method with parameters collection: %s, operation: %s, "
            "arguments: %s" % (collection, operation, kwargs))
        # connect to the database
        self.logger.debug("Connecting to database")
        mongo_db = self.connect()
        mongo_client = mongo_db.client
        self.logger.debug("Connected to database")

        # get the collection pointer - throw exception if not found
        mongo_collection = mongo_db[collection]
        if mongo_collection is None:
            raise UnsuccessfulOperationException(operation, **kwargs)
        self.logger.debug("Got collection %s" % collection)

        # try the operation - repeat MAX_RETRIES times
        operation_successful = False
        attempt = 1
        result = None
        while not operation_successful and attempt <= self.MAX_RETRIES:
            self.logger.debug("Attempting %s operation. Attempt %s of %s" % (
                operation, attempt, self.MAX_RETRIES))
            try:
                # perform operation
                if operation is DBOperation.INSERT:
                    self.logger.debug("attempting mongo_collection.insert_one")
                    try:
                        result = mongo_collection.insert_one(kwargs)
                        operation_successful = result.acknowledged
                        self.logger.debug(
                            "Attempt complete. Result = %s" % operation_successful)
                    except DuplicateKeyError:
                        self.logger.debug('Duplicate Key Error')
                        operation_successful = False
                        raise PrimaryKeyException()
                elif operation is DBOperation.FIND:
                    self.logger.debug("attempting mongo_collection.find")
                    result = mongo_collection.find(**kwargs).max_time_ms(50)
                    operation_successful = True
                    self.logger.debug(
                        "Attempt complete. Result = %s" % operation_successful)
                elif operation is DBOperation.UPDATE:
                    self.logger.debug("attempting mongo_collection.update_one")
                    result = mongo_collection.update_one(**kwargs)
                    operation_successful = result.acknowledged
                    self.logger.debug(
                        "Attempt complete. Result = %s" % operation_successful)
                elif operation is DBOperation.REPLACE:
                    self.logger.debug("attempting mongo_collection.replace_one")
                    result = mongo_collection.replace_one(**kwargs)
                    operation_successful = result.acknowledged
                    self.logger.debug(
                        "Attempt complete. Result = %s" % operation_successful)
                elif operation is DBOperation.DELETE:
                    self.logger.debug("attempting mongo_collection.delete_many")
                    result = mongo_collection.delete_many(**kwargs)
                    operation_successful = result.acknowledged
                    self.logger.debug(
                        "Attempt complete. Result = %s" % operation_successful)
            except UnsuccessfulConnectionException:
                # try again after INTERVAL_BETWEEN_RETRIES seconds
                self.logger.debug(
                    "Connection Failure. Trying again after %s seconds" % self.INTERVAL_BETWEEN_RETRIES)
                time.sleep(self.INTERVAL_BETWEEN_RETRIES)
                attempt += 1

        # disconnect from database
        self.logger.debug("Disconnecting from database")
        # self.disconnect(mongo_client)
        self.logger.debug("Disconnected successfully")

        # operation may not have succeeded even after MAX_RETRIES attempts
        if not operation_successful:
            self.logger.error(
                "Operation unsuccessful even after %s attempts" % self.MAX_RETRIES)
            raise UnsuccessfulOperationException(operation, **kwargs)

        self.logger.debug(
            "Exiting from perform_db_operation method with return value %s" % result)
        return result

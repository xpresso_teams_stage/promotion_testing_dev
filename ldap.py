import ldap
ldap_server = "ldap://127.0.0.1:30389"
connect = ldap.initialize(ldap_server)

user_dn = f"cn=admin,dc=abzooba,dc=com"

try:
    connect.bind_s(user_dn, "admin")
    res = connect.search_s("dc=abzooba, dc=com",ldap.SCOPE_SUBTREE)
    for dn, entry in res:
        print(dn)
        print(entry)
except ldap.INVALID_CREDENTIALS as e:
    self.logger.error("Invalid credentials provided : {}".format(e))
    raise LdapUserAdditionException("credentials are invalid")
except ldap.LDAPError as e:
    self.logger.error("Error : {}".format(e))
finally:
    connect.unbind_s()
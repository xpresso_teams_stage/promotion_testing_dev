builds=$(mongo localhost/xprdb --quiet --eval 'db.events.find({time:{$gte:new Date(new Date() - 7 * 60 * 60 * 24 * 1000)}, request_method: "POST", "request_type" : "/projects/build"}).pretty().count()')
echo "builds = $builds"

runs=$(mongo localhost/xprdb --quiet --eval 'db.events.find({time:{$gte:new Date(new Date() - 7 * 60 * 60 * 24 * 1000)}, request_method: "POST", "request_type" : "/experiments/start"}).pretty().count()')
echo "dev runs = $runs"

projects=$(mongo localhost/xprdb --quiet --eval 'db.events.find({time:{$gte:new Date(new Date() - 7 * 60 * 60 * 24 * 1000)}, request_method: "POST", "request_type" : "/projects/manage"}, {_id:0, user:1}).pretty()')
echo "last week's projects = $projects"

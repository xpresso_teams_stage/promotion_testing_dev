from pymongo import MongoClient
from datetime import datetime
import csv
import sys

client = MongoClient("localhost", 27017)
db = client.xprdb
collection = db.events

users_list = ["ralph.maison", "chanaka.munasinghe", "david.henderson", "vincenzo.digennaro", "philip.doherty", "alex.collins", "oliver.bickley", "tony.hubbard", "paul.barrett", "james.donald", "eleftherios.kollias", "weijin.tan","katarina.cohrs", "iain.maccormick", "robin.dederding", "ravi.baid", "miles.savva", "subodh.baid", "ralph.mahood", "aaron.harper", "evaldas.langvinis", "khan.follina", "michael.palframan"]
results = []
for user in users_list:
    users_info = collection.find({"request_type":"/auth", "request_method":"POST", "response.results.user_context.user_info.uid":user}).sort("_id", -1).limit(1)
    ui = list(users_info)
    if not ui:
        results.append([user, "Never logged in"])
        continue
    results.append([user,ui[0]["time"].strftime("%d/%m/%y")])

with open('/home/sahilm/users_info.csv', 'w+') as f:
    writer = csv.writer(f)
    writer.writerows(results)

#! /bin/bash

monitor_folder=/home/smb_user/sftp_data/outbound
archive_folder=/home/smb_user/sftp_data/archive
sftp_host=xpresso@sftp.avivainvestors.com
logfile=/opt/sftp_upload/logs/sftp.log
logfile_err=/opt/sftp_upload/logs/sftp.err

file_command(){
        echo "put \"$path$file\"" | sftp -i /opt/sftp_upload/key_converted $sftp_host
}

upload_file_to_sftp(){

        if file_command; then
                 echo "$(date) :: $file :: FILE UPLOADED" >> $logfile
                 sleep 1
                 mv "$path/$file" $archive_folder
                 echo "$(date) :: $file :: ARCHIVED" >> $logfile
        else
                 echo "$(date) :: $file :: FAILED" >> $logfile_err
        fi
}

while : ; do
        inotifywait -m $monitor_folder -e create | while read path action file; do
                if [ $action = "CREATE" ]; then
                        echo "$(date) :: $file :: $action :: $path" >> $logfile
                        upload_file_to_sftp
                fi
        done
done

"deployment_default_env_config": [
            {"controller": {"server_url": "str"}},
            {"logging": {"log_handler": {"host": "str"}}},
            {"logging": {"log_handler": {"port": "int"}}},
            {"pachyderm_server": {"cluster_ip": "str"}},
            {"pachyderm_server": {"port": "int"}},
            {"data_versioning": {"server": {"cluster_ip": "str"}}},
            {"data_versioning": {"server": {"port": "int"}}}
        ]
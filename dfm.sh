#! /bin/bash

monitor_folder=/home/smb_user/sftp_data/inbound
target_folder_rds=/Y-drive/Data/Aviva_Scheduled_Files/frontofficefiles_rds/
target_folder_aims=/Y-drive/Data/Aviva_Files_Other_MAM/AIMS/
target_folder_exception_configs=/Y-drive/ConfigFiles/
target_folder_market_regime=/Y-drive/Data/Market_Regime/
target_folder_mafi_global=/Y-drive/RDederding/MAFI\ Daily\ Folder/
target_folder_mafi_global_copy=/Y-drive/StreamlitVM10_MAF/MAF_DASHBOARD/MAFI\ Daily\ Folder/
target_folder_bnm=/home/smb_user/nfs_data/projects/aviva-bm/pipelines/data-processing/input_files/
logfile=/opt/daily_file_move/logs/file_move.log


move_file_to_target_folder(){

        if [[ "$file" =~ FrontOfficePositions.* ]]; then
                 sleep 1
                 FILESIZE=$(stat -c%s "$path/$file")
                 mv -f "$path/$file" $target_folder_rds
                 echo "$(date) :: $file :: $FILESIZE bytes :: MOVED TO $target_folder_rds" >> $logfile
        fi
        if [[ "$file" =~ Funds.Monitor_TR.*|Funds.Monitor_TI.*|ytdpnl_ti.*|ytdpnl_tr.*|TradesTTI.*|TradesTTR.* ]]; then
                 sleep 1
                 FILESIZE=$(stat -c%s "$path/$file")
                 mv -f "$path/$file" $target_folder_aims
                 echo "$(date) :: $file :: $FILESIZE bytes :: MOVED TO $target_folder_aims" >> $logfile
        fi
        if [[ "$file" =~ tbl.*|HMC_EDM.*|BMSM_EDM.*|CE_tbl.*|AI_TBL.*|AI_tbl.* ]]; then
                 sleep 1
                 FILESIZE=$(stat -c%s "$path/$file")
                 mv -f "$path/$file" $target_folder_bnm
                 echo "$(date) :: $file :: $FILESIZE bytes :: MOVED TO $target_folder_bnm" >> $logfile
        fi
        if [[ "$file" =~ marketregime.csv ]]; then
                 sleep 1
                 FILESIZE=$(stat -c%s "$path/$file")
                 mv -f "$path/$file" $target_folder_market_regime
                 echo "$(date) :: $file :: $FILESIZE bytes :: MOVED TO $target_folder_market_regime" >> $logfile
        fi
        if [[ "$file" =~ Xpresso_Exceptions_Configs.zip ]]; then
                 sleep 1
                 unzip -o "$path/$file" -d $target_folder_exception_configs
                 mv -f "$path/$file" $target_folder_exception_configs
                 echo "$(date) :: $file :: UNZIPPED & MOVED TO $"target_folder_exception_configs >> $logfile
        fi
        if [[ "$file" =~ mafi_global_.* ]]; then
                 sleep 1
                 FILESIZE=$(stat -c%s "$path/$file")
                 mv -f "$path/$file" "$target_folder_mafi_global"
                 sleep 1
                 cp -f "$target_folder_mafi_global/$file" "$target_folder_mafi_global_copy"
                 echo "$(date) :: $file :: $FILESIZE bytes :: MOVED TO $target_folder_rds; COPIED TO $target_folder_mafi_global_copy" >> $logfile
        fi

}

while : ; do
        inotifywait -m $monitor_folder -e create | while read path action file; do
                if [ $action = "CREATE" ]; then
                        move_file_to_target_folder
                fi
        done
done




import requests
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidConfigException
from xpresso.ai.core.commons.utils.constants import VAULT_IP_SUFFIX_XPRESSO, \
    VAULT_RESPONSE_DATA_KEY, VAULT_PASSWORDS_CACHE_KEY
from xpresso.ai.core.commons.utils.xpr_cache import XprCache
from xpresso.ai.core.commons.utils.singleton import Singleton


__all__ = ['XprVault']
__author__ = ['Sahil Malav']


class XprVault:

    def __init__(self, vault_ip, vault_port, request_headers, max_length,
                 max_size):
        self.ip = vault_ip
        self.port = vault_port
        self.headers = request_headers
        self.cache = self.VaultCache(max_length=max_length, max_size=max_size)

    def fetch_passwords(self):
        """
        Fetches passwords from vault by fetching from the cache or making
        a request to the vault server (if cache is empty)
        """
        pwd = self.cache.fetch_passwords_from_cache()
        if not pwd:
            response = requests.get(
                f'{self.ip}:{self.port}{VAULT_IP_SUFFIX_XPRESSO}',
                headers=self.headers)
            if VAULT_RESPONSE_DATA_KEY not in response.json():
                raise InvalidConfigException(
                    'Failed to fetch passwords from the vault.')
            pwd = response.json()[VAULT_RESPONSE_DATA_KEY]
            self.cache.store_passwords_in_cache(passwords=pwd)
        return pwd

    class VaultCache(XprCache, metaclass=Singleton):

        def __init__(self, max_length, max_size):
            super().__init__(
                max_length=max_length,
                max_size=max_size
            )

        def fetch_passwords_from_cache(self):
            """ fetches the passwords stored in Vault's cache """
            return self.get(VAULT_PASSWORDS_CACHE_KEY)

        def store_passwords_in_cache(self, passwords):
            """
            Stores vault passwords in the cache
            Args:
                passwords(dict): dict of passwords

            Returns: Nothing
            """
            self.set(VAULT_PASSWORDS_CACHE_KEY, passwords)
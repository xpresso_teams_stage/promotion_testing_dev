import os
import re

from xpresso.ai.core.commons.utils.constants import EMPTY_STRING

MOUNT_PATH = os.environ.get("XPRESSO_MOUNT_PATH", EMPTY_STRING)
METRICS_FOLDER = os.path.join(MOUNT_PATH, "reported_data/metrics")
PLOTS_FOLDER = os.path.join(MOUNT_PATH, "reported_data/plots")

MODEL_TYPE_ARG = "model_type"
PREDICTION_THRESHOLD_ARG = "prediction_threshold"
GENERATE_VALIDATION_METRICS_ARG = "generate_validation_metrics"
VALIDATION_SIZE_ARG = "validation_size"
OUTPUT_PATH_ARG = "output_path"

REGRESSOR = "regressor"
CLASSIFIER = "classifier"
UNSUPERVISED_CLUSTERING = "unsupervised_clustering"
BINARY = "binary"

CLASSIFIER_TARGET_TYPES = ["binary", "multiclass", "multiclass-multioutput",
                           "multilabel-indicator"]
# mutliclass or multilabel indicators
MULTI_INDICATOR = ["multiclass", "multiclass-multioutput",
                   "multilabel-indicator"]


def false_positive_rate(fp, tn):
    """
    Args:
        fp: False Positives (count)
        tn: True Negatives (count)

    Returns: false_positive_rate
    """
    return round(fp / (fp + tn), 2)


def false_negative_rate(fn, tp):
    """
    Args:
        fn: False Negatives (count)
        tp: True Positives (count)

    Returns: false_negative_rate
    """
    return round(fn / (fn + tp), 2)


def true_negative_rate(tn, fp):
    """
    Args:
        tn: True Negatives (count)
        fp: False Positives (count)

    Returns: true_negative_rate
    """
    return round(tn / (tn + fp), 2)


def format_metric_key(metric):
    """
    Reformat metrics name to make sure metric from different libraries to
    be consistent
    Args:
        metric: string, metrics name
    Returns: reformatted metrics name
    """

    if "tensorflow.python.keras" in metric.__module__:
        metric_name = metric.__class__.__name__
        metrics_name_split = [w for w in
                              re.split("([A-Z][^A-Z]*)", metric_name) if w]
        reformated_name = " ".join(metrics_name_split)
    else:
        metric_name = metric.__name__
        removed_punct = metric_name.replace("_", " ")
        reformated_name = " ".join(
            [w.capitalize() for w in removed_punct.split()])
    return reformated_name

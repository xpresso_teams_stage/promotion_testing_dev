"""
    Abstract base class for generation of model monitoring metrics
"""

__all__ = ["AbstractMetricGenerator"]
__author__ = ["Shlok Chaudhari"]


from abc import ABCMeta, abstractmethod
from xpresso.ai.core.logging.xpr_log import XprLogger


class AbstractMetricGenerator(metaclass=ABCMeta):
    """
        This is an interface class for generating
        different kinds of metrics required for
        model monitoring
    """

    _logger = XprLogger()

    def __init__(self, model_id, metric_type):
        self._model_id = model_id
        self._metric_type = metric_type

    @abstractmethod
    def _generate_metrics(self, *args, **kwargs):
        """ Abstract method to calculate or bring together all the metrics """

    @abstractmethod
    def get_metrics(self, *args, **kwargs):
        """ Abstract method to return all the calculated metrics """

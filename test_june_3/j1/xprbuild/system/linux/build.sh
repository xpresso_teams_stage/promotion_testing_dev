#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

# Install requirements
while IFS=" " read -r package; 
do 
  Rscript -e "install.packages('"$package"')" 
done < ${ROOT_FOLDER}/requirements/requirements.txt
